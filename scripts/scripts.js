(function($) {
	$(document).ready(function() {

        // owl carousel init
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            items: 4,
            dots: false,
            margin: 10,
            loop: true,
            center:true,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2
                },
                768: {
                    items: 2
                },
                992: {
                    items: 3
                },
                1200: {
                    items: 4
                },
                1600: {
                    items: 6
                }
            },
        });
        // go to the next item
        $('.owl-icon-next').click(function() {
            owl.trigger('next.owl.carousel');
        })
        // go to the previous item
        $('.owl-icon-prev').click(function() {
            // with optional speed parameter
            // parameters has to be in square bracket '[]'
            owl.trigger('prev.owl.carousel', [300]);
        })
        
	});	
})(jQuery);